package com.example.android.myapplication3;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button number0, number1, number2, number3, number4, number5, number6, number7, number8, number9;
    Button decimal, equals, clear, divide, multiply, add, substract,negative,percent;
    TextView result;
    String currentnumber;
    String firstnumber;
    Operand currentOperand;

    enum Operand{
        DIVIDE,ADD,SUBSTRACT,MULTIPLY,NONE
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initVar();
        initListener();

    }
    void initVar(){
        number0 = findViewById(R.id.button0);
        number1 = findViewById(R.id.button1);
        number2 = findViewById(R.id.button2);
        number3 = findViewById(R.id.button3);
        number4 = findViewById(R.id.button4);
        number5 = findViewById(R.id.button5);
        number6 = findViewById(R.id.button6);
        number7 = findViewById(R.id.button7);
        number8 = findViewById(R.id.button8);
        number9 = findViewById(R.id.button9);
        decimal = findViewById(R.id.decimal_button);
        equals = findViewById(R.id.equals_button);
        clear = findViewById(R.id.clear_button);
        divide = findViewById(R.id.divide_button);
        multiply = findViewById(R.id.multiply_button);
        add = findViewById(R.id.plus_button);
        substract = findViewById(R.id.minus_button);
        result=findViewById(R.id.display);
        negative = findViewById(R.id.negativeplus);
        percent =findViewById(R.id.percentage);

        currentnumber ="0";
        currentOperand =Operand.NONE;
        firstnumber ="0";


    }

    void initListener() {
        number0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendNumber(0);
            }
        });
        number1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendNumber(1);
            }
        });
        number2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendNumber(2);
            }
        });
        number3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendNumber(3);
            }
        });
        number4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendNumber(4);
            }
        });
        number5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendNumber(5);
            }
        });
        number6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendNumber(6);
            }
        });
        number7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendNumber(7);
            }
        });
        number8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendNumber(8);
            }
        });
        number9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendNumber(9);
            }
        });
        equals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calculate();
            }
        });
        decimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appenddecimal();
            }
        });
        multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                useOperand(Operand.MULTIPLY);

            }
        });
        substract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                useOperand(Operand.SUBSTRACT);

            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                useOperand(Operand.ADD);

            }
        });
        divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                useOperand(Operand.DIVIDE);

            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clear();
            }
        });
        percent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendpercent();
            }
        });
        negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendnegative();
            }
        });
    }
    void appendNumber(int number) {
        if (currentnumber.equals("0")) {
            currentnumber = String.valueOf(number);
        } else {
            currentnumber += String.valueOf(number);
        }
        updateResult();
    }

    void clear() {
        currentnumber = "0";
        updateResult();
    }

    void appendnegative() {
        int positivenumber = Integer.parseInt(currentnumber);
        int negativenumber = 0 - positivenumber;
        currentnumber = String.valueOf(negativenumber);
        updateResult();
    }
    void appenddecimal(){
        if(!currentnumber.contains(".")&&(currentnumber.length()>0)){
            currentnumber += ".";
        }
        updateResult();
    }
    void appendpercent(){
        int percentage = Integer.parseInt(currentnumber);
        float percent = (percentage/100f);
        currentnumber = String.valueOf(percent);
        updateResult();
    }

    void updateResult (){
        float currentNumberFloat = Float.parseFloat(currentnumber);
        if(currentNumberFloat<0){
           result.setTextColor(Color.parseColor("#f45942"));
        }else{
            result.setTextColor(Color.WHITE);
        }
        result.setText(currentnumber);
    }
    void useOperand(Operand operand) {
        if (currentOperand != Operand.NONE){
            calculate();
        }
        currentOperand = operand;
        firstnumber =currentnumber;
        currentnumber="0";

        updateResult();
    }
    private void calculate(){
        float numberOne = Float.parseFloat(firstnumber);
        float numberTwo = Float.parseFloat(currentnumber);
        float result =0;
        switch (currentOperand){
            case ADD:
                result = numberOne +numberTwo;
                break;
            case DIVIDE:
                result = numberOne/numberTwo;
                break;
            case MULTIPLY:
                result = numberOne*numberTwo;
                break;
            case SUBSTRACT:
                result = numberOne - numberTwo;
                break;
            default:
                result =numberTwo;
        }
        currentnumber = String.valueOf(result);
        currentOperand =Operand.NONE;
        firstnumber =currentnumber;
        if(Math.floor(result)== result){
            currentnumber =currentnumber.substring(0,currentnumber.length()-2);
        }

        updateResult();
    }


}
